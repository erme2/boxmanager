function aPage(xPage, xDiv, xPush, xForm){
    if(!xPage){
        return;
    }
    if(!xDiv){
        return;
    }

    // logging variables to xForm
    var xData = {};

    if(xForm){
        // array
        if(xForm.constructor.toString() == 'function Object() { [native code] }') {
            xData = xForm;
        }
        // string
        if(typeof xForm == 'string') {
            xData = verForm(xForm);
        }
    }

    xRequest = {
        url: xPage,
        container: xDiv,
        data: xData,
        success: function(xData){
            jQuery(xDiv).html(xData);
        },
        error: function(textStatus){
            alert('Errore'+textStatus.status+'! '+textStatus.statusText+' ('+xPage+')');
        },
        type: 'POST'
    }

    if(xPush){
        // pushing status
        jQuery.pjax(xRequest);
    } else {
        jQuery.ajax(xRequest);
    }
}

function verForm(xForm){

    var rEturn = {};
    if(!xForm){
        return {xForm: 'empty'};
    }
    // verifica text
    jQuery(xForm+' input').each(function() {
        rEturn[this.name] = this.value;
    });

    jQuery(xForm).each(function(){
        rEturn[this.name] = this.value;
    });
    if(!rEturn){return false;}

    jQuery(xForm+' select').each(function(){
        rEturn[this.name] = this.value;
    });

    jQuery(xForm+' :checkbox').each(function(){
        if(this.checked){
            rEturn[this.name] = 1;
        } else {
            rEturn[this.name] = 0;
        }
    });
    return rEturn;
}
