jQuery(document).on("click", "*.aLink", function (e) {
    e.preventDefault();

    var confMes = jQuery(this).attr('confirm_message'),
        href    = jQuery(this).attr('href'),
        xdiv    = jQuery(this).attr('xdiv'),
        xpush   = jQuery(this).attr('xpush'),
        xform   = jQuery(this).attr('xform'),
        res     = true;

    if(xdiv == '#main' && href=="/") {
        location.href=href;
        return;
    }

    if(confMes) {
        res = confirm(confMes);
    }

    if(res) {
        aPage(href, xdiv, xpush, xform);
    }
});

jQuery(document).on("click", "*.aLinkDev", function (e) {
    e.preventDefault();
    alert('Still working on this');
});
