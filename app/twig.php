<?php

$template = isset($app['config']['template']) ?
    $app['config']['template'] :
    "default";


$twigOptions = [
    'debug'     => $app['debug'],
    'template'  => $template
];

//setup Twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => "../templates/$template/twig",
    'twig.options' => $twigOptions
));

$app['twig']->addExtension(new Symfony\Bridge\Twig\Extension\TranslationExtension($app['translator']));
