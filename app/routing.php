<?php
$request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();

#main root
$app->get('/',          "controller.index:indexAction");
$app->post('/',         "controller.index:indexAction");
$app->post('/main',     "controller.index:mainAction");

# lang
$app->get('/lang/{_lang}', "controller.index:langAction");

#auth
$app->post('/auth',             "controller.index:loginAction");
$app->post('/auth/register',    "controller.index:registerAction");
$app->post('/auth/logout',      "controller.index:logoutAction");

#menu
$app->post('/menu',     "controller.menu:mainAction");

#boxes
$app->post('/boxes',        "controller.boxes:indexAction");

// TODO fix those routes
$app->post('/boxes/save/{id}',
    function ($id) use ($request, $app){
        return $app['controller.boxes']->saveAction($request, $app, $id);
    }
);
$app->post('/boxes/view/{id}',
    function ($id) use ($request, $app){
        return $app['controller.boxes']->viewAction($request, $app, $id);
    }
);
$app->post('/boxes/delete/{id}',
    function ($id) use ($request, $app){
        return $app['controller.boxes']->deleteAction($request, $app, $id);
    }
);
