<?php
$app->register(new Silex\Provider\SessionServiceProvider());

$expire = time() + $app['config']['session']['cookie_expires_in'];
$app['session.storage.options'] = [
    "cookie_lifetime" => $expire,
];
