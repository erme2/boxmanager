<?php
$app->register(new Silex\Provider\RoutingServiceProvider);
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\ServiceControllerServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());


// linking controllers
$app['controller.index'] = function($app) {
    return new boxManager\Controllers\IndexController($app);
};
$app['controller.menu'] = function($app) {
    return new boxManager\Controllers\MenuController($app);
};
$app['controller.boxes'] = function($app) {
    return new boxManager\Controllers\BoxController($app);
};
