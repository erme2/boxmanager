<?php

// getting the config
use Symfony\Component\Yaml;

$app['config'] = Yaml\Yaml::parse(
    file_get_contents(__DIR__.'/config/config_'.APP_ENV.'.yml')
);



