<?php
require_once __DIR__.'/../vendor/autoload.php';

$app = new Silex\Application();
$app['debug'] = APP_ENV == "dev" ? true : false;

include 'config.php';

include 'sessions.php';

include 'controllers.php';

include 'locales.php';

include 'twig.php';

include 'routing.php';

$app->run();