// include gulp
var template = 'default',
    gulp = require('gulp'),
    cleanCSS = require('gulp-clean-css');

// Include more plugins
var plugins = require("gulp-load-plugins")({
    pattern: ['gulp-*', 'gulp.*', 'main-bower-files', 'pump'],
    replaceString: /\bgulp[\-.]/
});

var config = {
    uglifyOpt:  {preserveComments: 'license'},
    bowerDir:   './templates/'+template+'/bower/',
    libFixed:   './templates/'+template+'/libFix/',
    devJs:      './templates/'+template+'/js/',
    devCss:     './templates/'+template+'/css/',
    public_dest:'./web/'
};

// default gulp task - do it all
gulp.task('default',
    [
        'js',
        'css'
    ],
    function() {});

// js gulp task - do just JS
gulp.task('js',
    [
        'js_ie_libs',
        'js_load',
        'js_libs',
        'js_main'
    ],
    function() {});
// css gulp task - do just css
gulp.task('css',
    [
        'css_main',
        'css_template'
    ],
    function() {});

gulp.task('js_ie_libs', function (cb) {
    plugins.pump([
            gulp.src([
                config.bowerDir + '/html5shiv/dist/**.*.js',
                config.bowerDir + '/respond/dest/*.js'
            ]),
            plugins.concat('ie_libs.min.js'),
            plugins.uglify(),
            gulp.dest(config.public_dest + 'js')
        ]
    );
});

gulp.task('js_load', function (cb) {
    plugins.pump([
            gulp.src([
                config.bowerDir + '/jquery/dist/jquery.js',
                config.bowerDir + '/bootstrap/dist/js/bootstrap.js',
                config.libFixed + '/jquery-pjax/*.fixed.js'
            ]),
            plugins.concat('load.min.js'),
            plugins.uglify(config.uglifyOpt),
            gulp.dest(config.public_dest + 'js')
        ]
    );
});
gulp.task('js_libs', function (cb) {
    plugins.pump([
            gulp.src([
                config.bowerDir + '/bootstrap-validator/dist/**.*.js'
            ]),
            plugins.concat('libs.min.js'),
            plugins.uglify(config.uglifyOpt),
            gulp.dest(config.public_dest + 'js')
        ]
    );
});

gulp.task('js_main', function (cb) {
    plugins.pump([
            gulp.src([
                config.devJs + '/*.js'
            ]),
            plugins.concat('main.min.js'),
            plugins.uglify(config.uglifyOpt),
            gulp.dest(config.public_dest + 'js')
        ]
    );
});

gulp.task('css_main', function (cb) {
    plugins.pump([
            gulp.src([
                config.bowerDir + '/bootstrap/dist/css/*.css',
            ]),
            plugins.concat('main.min.css'),
            cleanCSS({keepSpecialComments:0}),
            gulp.dest(config.public_dest + 'css')
        ]
    );
});
gulp.task('css_template', function (cb) {
    plugins.pump([
            gulp.src([
                config.devCss + '*.css',
            ]),
            plugins.concat('theme.min.css'),
            cleanCSS({keepSpecialComments:0}),
            gulp.dest(config.public_dest + 'css')
        ]
    );
});