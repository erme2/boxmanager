<?php

namespace boxManager\Services;

use Symfony\Component\Yaml\Yaml;

class DataService
{
    const FILE_EXTENSION = "#.yml";

    var $archiveName    = false;
    var $archiveFolder  = __DIR__.'/../../../app/';
    var $uniqueKey      = '';

    public function __construct($archiveName)
    {
        $this->archiveName = $archiveName;
    }

    public function countRecords()
    {

        $dir = $this->archiveFolder.$this->archiveName.'/';

        if(is_dir($dir)){
            return count(
                $this->_getFilesList($dir)
            );
        }
        return 0;

    }

    public function getAll(){

        $return = [];

        $list = $this->_getFilesList(
            $this->archiveFolder.$this->archiveName.'/'
        );

        if(count($list) > 0) {
            foreach ($list as $item) {
                $id = str_replace(
                    [
                        $this->_getRecordCompleteFolder(),
                        $this::FILE_EXTENSION,
                    ]
                    ,"",$item);
                $return[$id] = $this->readRecord($id);
            }
        }

        return $return;
    }

    public function deleteRecord($id)
    {
        $fileName = $this->_getRecocordAddress($id);
        if (
            file_exists($fileName)
        ) {
            $unlink = unlink($fileName);
            if($unlink) {
                return true;
            } else {
                throw new \Exception('Delete failed');
            }
        } else {
            if (APP_ENV == 'dev') {
                throw new \Exception("Record $id does not exits");
            } else {
                throw new \Exception("Record does not exits");
            }
        }

    }

    public function readRecord($id)
    {
        $fileName = $this->_getRecocordAddress($id);

        if (
            file_exists($fileName)
        ) {
            $record = Yaml::parse(file_get_contents($fileName));
            $record['id'] = $id;
            return $record;
        } else {
            if (APP_ENV == 'dev') {
                throw new \Exception("Record $id does not exits");
            } else {
                throw new \Exception("Record does not exits");
            }
        }
    }

    public function writeRecord($id, Array $content, $overWrite = false)
    {
        if($id == 'new') {
            $id = uniqid('', false);
            $content['id'] = $id;
        }

        $fileName = $this->_getRecocordAddress($id);

        if(
            false === $overWrite &&
            file_exists($fileName)
        ) {
            throw new \Exception("Record exists");
        }

        $handle = fopen($fileName, 'w');

        if($handle) {
            fwrite($handle, "---\n");
            fwrite($handle,
            Yaml::dump($content));
            fclose($handle);
            return $id;
        } else {
            if(APP_ENV == 'dev'){
                $error = "Unable to open/create the Record ($fileName) ";
            } else {
                $error = "System Error :(";
            }
            throw new \Exception($error);
        }
    }

    private function _getRecordCompleteFolder()
    {
        return $this->archiveFolder.$this->archiveName.'/';
    }

    private function _getRecocordAddress($id){

        return $this->_getRecordCompleteFolder().$id.$this::FILE_EXTENSION;

    }

    private function _getFilesList($dir){

        return glob($dir.'*'.$this::FILE_EXTENSION);
    }

}