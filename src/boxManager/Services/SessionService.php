<?php
namespace boxManager\Services;


class SessionService extends AncestorService
{
    var $_expireTime = 10 * 60; // ten minutes

    public function __construct($app)
    {
        $this->app = $app;
        $this->session = isset($app['session']) ?
            $app['session'] : false;
        if(
            isset($app['config']['session_expires_in']) &&
            abs($app['config']['session_expires_in']) > 0
        ) {
            $this->_expireTime = $app['config']['session_expires_in'];
        }
    }

    public function check()
    {
        if($this->session) {
            if($this->session->get('last_action')){
                $interval = time() - $this->session->get('last_action');
                if($interval > $this->_expireTime){
                    $this->close();
                    return false;
                }
                $this->session->set('last_action', time());
                $lang = $this->session->get('lang');
                if($lang) {
                    $this->app['translator']->setLocale($lang);
                }
                return $this->session;
            }
            return false;
        } else {
            return false;
        }
    }

    public function open($user)
    {
        $this->session->set('name', $user['name']);
        $this->session->set('email', $user['email']);
        $this->session->set('last_action', time());
    }

    public function close() {
        $this->session->clear();
    }

    public function set($key, $value)
    {
        // TODO Add try/catch exception here!
        return $this->session->set($key, $value);
    }

    public function get($key)
    {
        // TODO Add try/catch exception here!
        return $this->session->get($key);
    }
}