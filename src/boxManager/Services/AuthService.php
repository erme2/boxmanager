<?php
namespace boxManager\Services;


use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;


class AuthService extends AncestorService
{
    var $archiveName = 'users';

    public function haveUsers() {
        return $this->data->countRecords();
    }

    public function login(Request $request, Application $app)
    {
        $email = $request->get('inputEmail');
        $pass1 = $request->get('inputPassword');

        // check email
        $errors = $app['validator']->validate($email, new Assert\Email());
        if($errors->count() > 0){
            throw new \Exception("Invalid Email Address");
        }

        // find user
        try {
            $user = $this->data->readRecord($email);
        } catch (\Exception $e) {
            throw new \Exception("Login failed");
        }

        // check password
        if(password_verify($pass1, $user['pass'])){
            return $user;
        } else {
            throw new \Exception("Login failed");
        }
    }

    public function updateUser(Request $request, Application $app)
    {
        $name = $request->get('inputName');
        $email = $request->get('inputEmail');
        $pass1 = $request->get('inputPassword');
        $pass2 = $request->get('inputPasswordConfirm');

        // check name
        if(strlen(trim($name)) <= 2) {
            throw new \Exception("Name too short");
        }

        // check email
        $errors = $app['validator']->validate($email, new Assert\Email());
        if($errors->count() > 0){
            throw new \Exception("Wrong Email Address");
        }

        // check password
        if($pass1 !== $pass2) {
            throw new \Exception("Whoops, passords don't match");
        }

        try {
            if(
                $this->data->writeRecord(
                    $email,
                    [
                        'name'  => $name,
                        'email' => $email,
                        'pass'  => password_hash($pass1, PASSWORD_DEFAULT),
                    ]
                )
            ) {
                return "User $email created";
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

}