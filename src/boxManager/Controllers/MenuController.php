<?php

namespace boxManager\Controllers;


class MenuController extends AncestorController
{

    public function mainAction() {
        $session = $this->session->check();
        if($session) {
            $twigArray = $session->all();
            return $this->app['twig']->render('menu/main.html.twig', $twigArray);
        } else {
             return $this->sessionFailed();
        }
    }

}