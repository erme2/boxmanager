<?php

namespace boxManager\Controllers;

// TODO ADD user groups
// TODO SAVE box per user group

use boxManager\Services\DataService;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class BoxController extends AncestorController
{
    var $archiveName =  'boxes';

    public function indexAction ()
    {
        if($this->session->check()){
            $twigArray = [
                'labels'    => [
                    'id'        => ['label' => '#ID'],
                    'name'      => ['label' => 'Name'],
                    'address'   => ['label' => 'URL Address'],
                    'saltkey'   => ['label' => 'SaltKey'],
                    'checkSSL'  => ['label' => 'Check SSL'],
                ],
                'boxes'     => $this->data->getAll(),
            ];
            return $this->app['twig']->render('box/list.html.twig', $twigArray);

        } else {
            return $this->sessionFailed();
        }


    }

    public function deleteAction (Request $request, Application $app, $id)
    {
        if($this->session->check()) {
            $twigArray = $this->getSystemMessages($request);

            try {
                $this->data->deleteRecord($id);
                $twigArray['message'] = "$id successfully deleted";
            } catch (\Exception $e) {
                $twigArray['error'] = $e->getMessage();
            }

            return $this->loadPage("/boxes", '#main_content', false, $twigArray);
        } else {
            return $this->sessionFailed();
        }


    }

    public function saveAction (Request $request, Application $app)
    {
        if($this->session->check()){

            $id = $request->get('inputId');
            $overWrite = false;

            try {
                $box = $this->data->readRecord($id);
                $overWrite = true;
            } catch (\Exception $e) {
                // new record
                $box = ["id"=>"new"];
            }

            $box['name']        = trim($request->get('inputName'));
            $box['address']     = $request->get('inputAddress');
            $box['saltkey']     = $request->get('inputSalt');
            $box['checkSSL']    = $request->get('inputSSL');

            try {
                $this->_checkBoxData($app, $box);
            } catch (\Exception $e) {
                return $this->loadPage(
                    "/boxes/view/new",
                    '#main_content',
                    false,
                    [
                        'error'=>$e->getMessage(),
                        'name' => $box['name'],
                        'address' => $box['address'],
                        'saltkey' => $box['saltkey'],
                        'checkSSL' => $box['checkSSL'],
                    ]
                );
            }

            try {
                $newID = $this->data->writeRecord($id, $box, $overWrite);
            } catch (\Exception $e){
                return $this->loadPage("/boxes/view/new", '#main_content', false, [
                    'error'=>$e->getMessage(),
                    'name' => $box['name'],
                    'address' => $box['address'],
                    'saltkey' => $box['saltkey'],
                    'checkSSL' => $box['checkSSL'],
                ]);
            }
            return $this->loadPage("/boxes/view/$newID", '#main_content', false, ['message'=>$box['name']." saved!"] );
        } else {
            return $this->sessionFailed();
        }
    }

    public function viewAction (Request $request, Application $app, $id)
    {
        if($this->session->check()){
            $twigArray = $this->getSystemMessages($request);

            try {
                $twigArray['box'] = $this->data->readRecord($id);
            } catch (\Exception $e) {
                $twigArray['box'] = ["id" => "new"];
            }
            $twigArray['boxStatusAtGit'] = $this->app['config']['boxStatus']['gitUrl'];
            return $this->app['twig']->render('box/view.html.twig', $twigArray);

        } else {
            return $this->sessionFailed();
        }
    }

    private function _checkBoxData($app, $box){
        // name
        $errors = $app['validator']->validate(trim($box['name']), new Assert\NotBlank());
        if($errors->count() > 0){
            throw new \Exception('Name can not be blank.');
        }
        // address
        $errors = $app['validator']->validate(trim($box['address']), new Assert\Url());
        if($errors->count() > 0){
            throw new \Exception('Invalid URL Address');
        }
        return true;
    }

}