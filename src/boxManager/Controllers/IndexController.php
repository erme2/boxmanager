<?php
namespace boxManager\Controllers;

use boxManager\Services\AuthService;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends AncestorController
{
    /*
     * loading main frame
     */
    public function indexAction(Application $app)
    {
        return $app['twig']->render('index/index.html.twig',
            [
                'time' => time(),
                'app' => $app['config']['app']
            ]
        );
    }

    public function langAction(Request $request)
    {

        $lang = $request->get('_lang');
        $localesPath = $_SERVER['DOCUMENT_ROOT'].$this->app['config']['locales']['path']."$lang.yml";
        if(file_exists($localesPath)){
            $this->session->set('lang', $lang);
        }

        return $this->app->redirect('/');
    }

    public function loginAction(Request $request)
    {
        //do you have any user saved?
        $auth = new AuthService();
        if($auth->haveUsers()){
            $twigArray = $this->getSystemMessages($request);

            if($request->get('inputEmail')){
                try {
                    $user = $auth->login($request, $this->app);
                } catch (\Exception $e){
                    return $this->loadPage('/auth','#main', false, ['error'=>$e->getMessage()]);
                }
                $this->session->open($user);
                return $this->loadPage('/', '#main');
            }
            return $this->app['twig']->render('index/login.html.twig', $twigArray);
        } else {
            return $this->app['twig']->render('index/register.html.twig', []);
        }
    }

    public function logoutAction(){
        $this->session->close();
        return $this->loadPage('/main', '#main');
    }

    public function mainAction(Application $app)
    {
        if($this->session->check()){
            return $app['twig']->render('index/main.html.twig',[]);

        } else {
            return $this->loadPage('/auth', '#main');
        }
    }

    public function registerAction(Request $request, Application $app)
    {
        $error = $request->get('error');

        if($error) {
            return $app['twig']->render('index/register.html.twig', [
                'error' => $error,
                'inputName' => $request->get('inputName'),
                'inputEmail' => $request->get('inputEmail'),
            ]);

        } else {
            $auth = new AuthService();
            try {
                $message = $auth->updateUser($request, $app);
                return $this->loadPage('/auth','#main', false, ['message'=>$message]);
            } catch (\Exception $e) {
                $return = [
                    'error' => $e->getMessage(),
                    'inputName' => $request->get('inputName'),
                    'inputEmail' => $request->get('inputEmail'),
                ];

                return $this->loadPage('/auth/register','#main', false, $return);
            }
        }




    }
}