<?php
namespace boxManager\Controllers;


use boxManager\Services\DataService;
use Silex\Application;
use boxManager\Services\SessionService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Translator;

class AncestorController
{
    var $archiveName    =  false;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->session = new SessionService($this->app);
        if($this->archiveName){
            $this->data = new DataService($this->archiveName);
        }
        $lang = $this->session->get('lang');
        if($lang) {
            $this->app['translator']->setLocale($lang);
        }

    }

    public function jsExec($js){
        return $this->app['twig']->render('common/jScript.html.twig',
            ['jScript' => "$js"]
        );
    }

    public function loadPage($pageEndPoint, $divID, $push = false, $formID  = false){
        $push   = $push     ? "true"        : "false";
        if($formID){
            // transforming a php array in to a JS obj
            if(is_array($formID)){
                $formID = json_encode($formID);
            } else {
                $formID = "'$formID'";
            }
        } else {
            $formID = "false";
        }
        return $this->jsExec("aPage('$pageEndPoint','$divID', $push, $formID);");
    }

    public function getSystemMessages(Request $request, Array $return = []){

        $search = ['message', 'alert', 'error'];
        foreach ($search as $item) {
            if($request->get($item)){
                $return[$item] = $request->get($item);
            }
        }

        return $return;
    }

    public function sessionFailed()
    {
        return $this->loadPage('/','#main');

    }

}